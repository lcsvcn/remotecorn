import React from 'react';

import { SafeAreaView } from 'react-navigation';

import { AppRegistry } from "react-native";
import { name as appName } from "./app.json";

import Routes from "./src";

AppRegistry.registerComponent(appName, () => Routes);

export default class App extends React.Component {
  render() {
    console.disableYellowBox = true;

    return (
        <SafeAreaView
          forceInset={{
            top: 'always',
            bottom: 'never',
          }}
          style={{ flex: 1 }}
        >
        </SafeAreaView>
    );
  }
}
