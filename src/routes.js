import React from 'react';

import { Icon } from 'react-native-elements'

import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { Home, Favoritos, Movies, Series, Settings} from './screens'

const Routes = createAppContainer(
  createMaterialBottomTabNavigator({
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: 'Controle',
        tabBarIcon: () => (
          <Icon name='settings-remote' color="#fff" size={24} />
        ),
      },
    },
    Favoritos: {
      screen: Favoritos,
      navigationOptions: {
        tabBarLabel: 'Favoritos',
        tabBarIcon: () =>   (
          <Icon name='favorite' type='material' color="#fff" size={24} />
        )
      }
    },
    Movies: {
      screen: Movies,
      navigationOptions: {
        tabBarLabel: 'Filmes',
        tabBarIcon: () =>   (
          <Icon name='movie' color="#fff" size={24} />
        )
      }
    },
    Series: {
      screen: Series,
      navigationOptions: {
        tabBarLabel: 'Series',
        tabBarIcon: () =>   (
          <Icon name='theaters' color="#fff" size={24} />
        )
      }
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        tabBarLabel: 'Configurações',
        tabBarIcon: () =>   (
          <Icon name='settings' color="#fff" size={24} />
        )
      }
    },
  },
  {
    initialRouteName: 'Home',
    shifiting: true,
    activeColor: '#F44336',
    inactiveColor: '#123123',
    barStyle: { backgroundColor: '#4fd23f' },
    order: ['Home', 'Favoritos', 'Movies', 'Series', 'Settings'],
    activeTintColor: "#fff",
    labeled: false
  })
);

export default Routes;
