import React from 'react';

import {
  SafeAreaView,
  Text
} from 'react-native';

import styles from './styles';

class Home extends React.Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text>Control</Text>
      </SafeAreaView>
    );
  }
};

export default Home;
