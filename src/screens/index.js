export {default as Home} from './home';
export {default as Favoritos } from './favoritos';
export {default as Series } from './series';
export {default as Movies } from './movies';
export {default as Settings} from './settings';
