import { StyleSheet, Dimensions, Platform } from 'react-native';

// get screen size
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default StyleSheet.create({

  container: {
    flex: 1,
    marginTop: 0,
    height:  Platform.OS === 'ios' ? screenHeight-60 : screenHeight-25,
    width: screenWidth,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
